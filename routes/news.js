var express = require('express');
var router = express.Router();
var Blog = require('../models/blog')

router.get('/one/:id', function(req, res) {
  Blog.findOne({
    _id: req.params.id
  }, function(err, one) {
    if (err) {
      console.log(err);
    } else {
      res.render('one', {
        news: one
      });
    };
  });
});

router.get('/', function(req, res) {
  Blog.find({}, function(err, collection) {
    if (err)
      console.log(err)
    else {
      res.json(collection);
    };
  });
});



router.get('/add', function(req, res) {
  res.render('add_news');
});

router.post('/add', function(req, res) {
  // var newBlog = new Blog({
  //   title: req.body.title,
  //   author: req.body.author,
  //   body: req.body.body
  // })

  Blog.create({
    title: req.body.title,
    author: req.body.author,
    body: req.body.body
  }, function(error, created) {
    if (error) return handleError(error);
    else res.json(created);
  })
  // newBlog.save(function(error, saved) {
  //   if (error) {
  //     console.log(error);
  //   } else res.send('your news was been saved');
  // })
});

router.get('/edit/:id', function(req, res) {
  Blog.findOne({
    _id: req.params.id
  }, function(err, edited) {
    if (err) {
      console.log(err);
    } else {
      res.json(edited);
    };
  });
});

router.post('/edit/:id', function(req, res) { //edit экземпляр объекта
  Blog.findOne({
    _id: req.params.id
  }, function(err, edit) {
    if (err) {
      console.log(err);
    } else {
      for (key in req.body) {
        edit[key] = req.body[key];
      }
      edit.save(function(error, saved) {
        if (error) {
          console.log(error);
        } else res.json(saved);
      })
    };
  });
});

router.get('/delete/:id', function(req, res) {
  res.render('delete');
});
router.post('/delete/:id', function(req, res) {
  Blog.deleteOne({
    _id: req.params.id
  }, function(err) {
    if (err) console.log(err);
    else {
      res.redirect('/news');
    }
  });
});
module.exports = router;