var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');
var blogSchema = new Schema({
  title: String,
  author: String,
  body: String,
  comments: [{
    body: String,
    date: Date
  }],
  date: {
    type: Date,
    default: Date.now
  },
  hidden: Boolean,
})

blogSchema
  .virtual('date_string')
  .get(function() {
    return moment(this.date).format('DD.MM.YYYY HH:mm')
  });
var Blog = mongoose.model('Blog', blogSchema);

module.exports = Blog;