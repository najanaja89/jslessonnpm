var express = require('express');
var cors = require('cors');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser')
mongoose.connect('mongodb://localhost/news');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))
var myLogger = function(req, res, next) {
  console.log('Logged');
  next();
};

app.set('views', './views');
app.set('view engine', 'jade');
app.use(cors());
app.use(myLogger);
app.use(express.static('public'));
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  extended: false,
  limit: '50mb',
  parameterLimit: 50000
}));
// app.get('/',function(req,res){
//     res.send('Hello world');
// });

var router = require('./routes/news');
app.use('/news', router);
app.get('/', function(req, res) {
  res.render('index', {
    title: 'Hey',
    message: 'Hello there!'
  });
});
app.get('/Hello', function(req, res) {
  res.send('Hello world2');
})



app.listen(3001, function() {
  console.log('Example app listening on port 3001!');
});
